# Ansible inventories
This repository stores hosts informations and related variables for this specific instance of Odoo.

## Requirements

1. Clone this repo and [odoo-provisioning](https://gitlab.com/coopdevs/odoo-provisioning) in the same directory
2. If you want to test this set up locally, install [devenv](https://github.com/coopdevs/devenv/) and do:
   ```sh
   cd odoo-ov-provisioning
   devenv # this creates the lxc container and sets its hostname
   ```
3. Go to `odoo-provisioning` directory and install its Ansible dependencies:
   ```sh
   ansible-galaxy install -r requirements.yml
   ```
4. Run `ansible-playbook` command pointing to the `inventory/hosts` file of this repository:
   * development local mode
   ```sh
   # tell it to keep it local with limit=dev
   # don't ask for vault pass as no secrets are required for local setups
   ansible-playbook playbooks/provision.yml -i ../odoo-ov-provisioning/inventory/hosts --limit=dev
   ```
   * production mode
   ```sh
   ansible-playbook playbooks/provision.yml -i ../odoo-ov-provisioning/inventory/hosts --ask-vault-pass --limit=production
   ```
5. In development visit http://odoo-ov.local:8069

## Odoo Core Modules

- [account](https://github.com/odoo/odoo/tree/11.0/addons/account)
- account
- account_analytic_default
- account_asset
- account_bank_statement_import
- account_cancel
- account_invoicing
- analytic
- auth_crypt
- auth_signup
- base
- base_automation
- base_iban
- base_import
- base_setup
- base_vat
- base_vat_autocomplete
- contacts
- decimal_precision
- document
- l10n_be
- product
- sale_management
- web
- web_diagram
- web_kanban_gauge
- web_planner
- web_settings_dashboard

## Odoo Community Modules

> Using the `requirements.txt` file in this repository
- account_banking_mandate
- account_banking_pain_base
- account_banking_sepa_credit_transfer
- account_banking_sepa_direct_debit
- account_due_list
- account_financial_report
- account_payment_order
- account_payment_partner
- base_bank_from_iban
- base_technical_features
- contract
- contract_sale_invoicing
- contract_variable_quantity
- web_favicon
- web_no_bubble
- web_responsive
- web_searchbar_full_width
- l10n_be_mis_reports

## Instances

* [Odoo OV](https://odoo-ov.coopdevs.org)
